package Project;


import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author siyuan
 * @date 2020/12/26 21:43
 */
public class View {
    static ArrayList<User> list = new ArrayList<>();
    static ArrayList<Phone> phones = new ArrayList<>();
    public static void main(String[] args) {
        //注册
        registered();
        //登录
        login();
        //主界面
        view();
    }
    //主界面
    public static void view(){
        Phone phone1 = new Phone("小米", 5.5, 2500, 20);
        Phone phone2 = new Phone("华为", 5.8, 2700, 40);
        Phone phone3 = new Phone("oppo", 6.2, 3000, 40);
        phones.add(phone1);
        phones.add(phone2);
        phones.add(phone3);
        while (true) {
            Scanner in = new Scanner(System.in);
            System.out.println("********库存管理系统**************");
            System.out.println("1.查看库存清单");
            System.out.println("2.修改商品库存数量");
            System.out.println("3.清空库存");
            System.out.println("4.添加商品");
            System.out.println("5.根据商品属性修改");
            System.out.println("6.注册新用户");
            System.out.println("7.重新登陆");
            System.out.println("8.退出");

            System.out.println("请输入要执行的操作号");
            int enter = in.nextInt();
            if (enter == 1) {
                checkList();
            } else if (enter == 2) {
                changeList();

            } else if (enter == 3) {
                clearList();
            }else if(enter ==4 ) {
                addList();
            } else if (enter == 5) {
                changePhone();
            }else if (enter ==6){
                registered();
            }else if (enter==7){
                login();
            }else if (enter ==8)
            {
                System.out.println("系统退出");
                System.exit(500);
            }
            else {
                System.out.println("请输入正确的数字！");
            }
        }

    }
    //登录功能
    public static void login(){

        while (true) {
            Scanner in1 = new Scanner(System.in);
            System.out.println("********库存管理系统**************");
            System.out.println("请输入用户名:");
            String nametest = in1.next();
            System.out.println("请输入密码:");
            String passwordtest = in1.next();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getName().equals(nametest) && list.get(i).getPassword().equals(passwordtest)){
                    System.out.println("登陆成功");
                    view();
                }
            }
            System.out.println("账号或密码输入错误");

            if (list.size() ==0){
                System.out.println("未注册，程序退出");
                System.exit(500);
            }
            }
        }

    //注册功能
    public static void registered(){
        for (int i = 0; true; i++) {

            System.out.println("请注册:");
            User user = new User();
            System.out.println("******输入exit退出注册********");
            System.out.println("请输入用户名:");
            Scanner nameInput = new Scanner(System.in);
            String name = nameInput.next();
            if (i >0){
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).getName().equals(name)){
                        System.out.println("用户名已存在");
                        registered();
                    }
                }
            }
            if ("exit".equals(name)){
                System.out.println("退出成功");
                login();
                break;
            }
            System.out.println("请输入密码:");
            Scanner passwordInput = new Scanner(System.in);
            String password = passwordInput.next();
            user.setName(name);
            user.setPassword(password);
            list.add(user);
        }
    }
    public static void checkList() {
        if (phones.size() == 0) {
            System.out.println("暂无商品");
        }
        int sumNumber = 0;

        System.out.println("************查看库存清单**************");
        System.out.println("品牌型号       尺寸           价格    库存数");
        for (int i = 0; i < phones.size(); i++) {

            System.out.println(i + "品牌型号" + phones.get(i).getBrand() + ",尺寸是" + phones.get(i).getSize()
                    + "价格为" + phones.get(i).getPrice() + "库存数为" + phones.get(i).getNumber());

            for (int j = 0; j < phones.size(); j++) {

                sumNumber += phones.get(j).getNumber();
            }
        }
        System.out.println("总库存数为" + sumNumber);


    }

    public static void changeList() {
        Scanner in = new Scanner(System.in);
        System.out.println("修改商品的库存数量：");
        for (int i = 0; i < phones.size(); i++) {
            System.out.println("请输入" + phones.get(i).getBrand() + "商品库存数" + "///" + "当前库存数为" + phones.get(i).getNumber());
            int enterNumber = in.nextInt();
            phones.get(i).setNumber(enterNumber);

        }

    }
    public static void clearList(){
        Scanner in = new Scanner(System.in);
        checkList();
        System.out.println("请选择清空的商品编号");
        int enterNumber = in.nextInt();

        System.out.println("确认清空吗？ y/n");
        String whether = in.next();

        if ("y".equals(whether)){
            if (enterNumber > phones.size()){
                System.out.println("输入的编号错误");
            }
            else {
                phones.remove(enterNumber);

            }
        }else
        {
            checkList();
        }

    }
    public static void addList(){
        System.out.println("请输入要添加的商品");
        System.out.println("请输入商品品牌:");
        Scanner in = new Scanner(System.in);
        String brand = in.next();
        Phone phone4 = new Phone();
        phone4.setBrand(brand);
        System.out.println("请输入商品尺寸:");
        double size = in.nextDouble();
        phone4.setSize(size);
        System.out.println("请输入商品价格:");
        double price = in.nextDouble();
        phone4.setPrice(price);
        System.out.println("请输入商品数量:");
        int number = in.nextInt();
        phone4.setNumber(number);
        phones.add(phone4);
        checkList();
    }
    public static void changePhone(){
        checkList();
        System.out.println("请输入修改的商品编号:");
        Scanner in = new Scanner(System.in);

        int enterNumber = in.nextInt();
        System.out.println("************查看库存清单**************");
        System.out.println("品牌型号       尺寸           价格    库存数");
        for (int i = 0; i < phones.size(); i++) {

            System.out.println(i + "品牌型号" + phones.get(i).getBrand() + ",尺寸是" + phones.get(i).getSize()
                    + "价格为" + phones.get(i).getPrice() + "库存数为" + phones.get(i).getNumber());
            if (enterNumber==i){
                System.out.println("请输入修改的属性");
                System.out.println("1.尺寸");
                System.out.println("2.价格");
                System.out.println("3.品牌");
                int enter = in.nextInt();

                switch (enter) {
                    case 1:
                        System.out.println("请输入修改的尺寸：");
                        double sizechange = in.nextDouble();
                        phones.get(enterNumber).setSize(sizechange);
                        break;
                    case 2:
                        System.out.println("请输入修改的价格：");
                        double pricechange = in.nextDouble();
                        phones.get(enterNumber).setPrice(pricechange);
                        break;
                    case 3:
                        System.out.println("请输入修改的品牌：");
                        String brandchange = in.next();
                        phones.get(enterNumber).setBrand(brandchange);
                        break;
                    default:
                        System.out.println("商品编号输入错误");
                        break;
                }
            }

        }
    }
}

