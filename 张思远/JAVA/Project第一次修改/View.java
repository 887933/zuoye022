package Project;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author siyuan
 * @date 2020/12/26 21:43
 */
public class View {
    public static void main(String[] args) {

        ArrayList<Phone> phones = new ArrayList();
        Phone phone1 = new Phone("小米", 5.5, 2500, 20);
        Phone phone2 = new Phone("华为", 5.8, 2700, 40);
        Phone phone3 = new Phone("oppo", 6.2, 3000, 40);
        User user1 = new User("张思远","123456");

        phones.add(phone1);
        phones.add(phone2);
        phones.add(phone3);
        Scanner in = new Scanner(System.in);
        Scanner in1 = new Scanner(System.in);
        System.out.println("请注册:");
        System.out.println("请输入用户名:");
        Scanner nameInput = new Scanner(System.in);
        String name = nameInput.next();
        System.out.println("请输入密码:");
        Scanner passwordInput = new Scanner(System.in);
        String password = passwordInput.next();
        user1.setName(name);
        user1.setPassword(password);

        while (true){
            System.out.println("********库存管理系统**************");
            System.out.println("请输入用户名:");
            String nametest = in1.next();
            System.out.println("请输入密码:");
            String passwordtest = in1.next();
            if (!nametest.equals(user1.getName())){
                System.out.println("用户名输入错误");
            }else if (!passwordtest.equals(user1.getPassword())){
                System.out.println("密码输入错误");
            }else {
                break;
            }
        }

        while (true) {
            System.out.println("********库存管理系统**************");
            System.out.println("1.查看库存清单");
            System.out.println("2.修改商品库存数量");
            System.out.println("3.清空库存");
            System.out.println("4.添加商品");
            System.out.println("5.根据商品属性修改");
            System.out.println("6.退出");

            System.out.println("请输入要执行的操作号");
            int enter = in.nextInt();
            if (enter == 1) {
                checkList(phones);
            } else if (enter == 2) {
                changeList(phones);

            } else if (enter == 3) {
                clearList(phones);
            }else if(enter ==4 ) {
                addList(phones);
            } else if (enter == 5) {
                changePhone(phones);
            }else if (enter ==6){
                break;
            }
            else {
                System.out.println("请输入正确的数字！");
            }
        }
    }

    public static void checkList(ArrayList<Phone> phones) {
        if (phones.size() == 0) {
            System.out.println("暂无商品");
        }
        int sumNumber = 0;
        double sumPrice = 0;
        System.out.println("************查看库存清单**************");
        System.out.println("品牌型号       尺寸           价格    库存数");
        for (int i = 0; i < phones.size(); i++) {

            System.out.println(i + "品牌型号" + phones.get(i).getBrand() + ",尺寸是" + phones.get(i).getSize()
                    + "价格为" + phones.get(i).getPrice() + "库存数为" + phones.get(i).getNumber());

            for (int j = 0; j < phones.size(); j++) {

                sumNumber += phones.get(j).getNumber();
            }

            for (int j = 0; j < phones.size(); j++) {

                sumPrice += phones.get(j).getPrice();
            }
            sumPrice = sumPrice * sumNumber;


        }
        System.out.println("总库存数为" + sumNumber);
        System.out.println("总金额为" + sumPrice);

    }

    public static void changeList(ArrayList<Phone> phones) {
        Scanner in = new Scanner(System.in);
        System.out.println("修改商品的库存数量：");
        for (int i = 0; i < phones.size(); i++) {
            System.out.println("请输入" + phones.get(i).getBrand() + "商品库存数" + "///" + "当前库存数为" + phones.get(i).getNumber());
            int enterNumber = in.nextInt();
            phones.get(i).setNumber(enterNumber);

        }

    }
    public static void clearList(ArrayList<Phone> phones){
        Scanner in = new Scanner(System.in);
        checkList(phones);
        System.out.println("请选择清空的商品编号");
        int enterNumber = in.nextInt();

        System.out.println("确认清空吗？ y/n");
        String whether = in.next();

        if (whether.equals("y")){
            phones.remove(enterNumber);
        }else
        {
            checkList(phones);
        }

    }
    public static void addList(ArrayList<Phone> phones){
        System.out.println("请输入要添加的商品");
        System.out.println("请输入商品品牌:");
        Scanner in = new Scanner(System.in);
        String brand = in.next();
        Phone phone4 = new Phone();
        phone4.setBrand(brand);
        System.out.println("请输入商品尺寸:");
        Double size = in.nextDouble();
        phone4.setSize(size);
        System.out.println("请输入商品价格:");
        Double price = in.nextDouble();
        phone4.setPrice(price);
        System.out.println("请输入商品数量:");
        int number = in.nextInt();
        phone4.setNumber(number);
        phones.add(phone4);
        checkList(phones);
    }
    public static void changePhone(ArrayList<Phone> phones){
        checkList(phones);
        System.out.println("请输入修改的商品编号:");
        Scanner in = new Scanner(System.in);

        int enterNumber = in.nextInt();
        System.out.println("************查看库存清单**************");
        System.out.println("品牌型号       尺寸           价格    库存数");
        for (int i = 0; i < phones.size(); i++) {

            System.out.println(i + "品牌型号" + phones.get(i).getBrand() + ",尺寸是" + phones.get(i).getSize()
                    + "价格为" + phones.get(i).getPrice() + "库存数为" + phones.get(i).getNumber());
            if (enterNumber==i){
                System.out.println("请输入修改的属性");
                System.out.println("1.尺寸");
                System.out.println("2.价格");
                System.out.println("3.品牌");
                int enter = in.nextInt();

                switch (enter) {
                    case 1:
                        System.out.println("请输入修改的尺寸：");
                        double sizechange = in.nextDouble();
                        phones.get(enterNumber).setSize(sizechange);
                        break;
                    case 2:
                        System.out.println("请输入修改的价格：");
                        double pricechange = in.nextDouble();
                        phones.get(enterNumber).setPrice(pricechange);
                        break;
                    case 3:
                        System.out.println("请输入修改的品牌：");
                        String brandchange = in.next();
                        phones.get(enterNumber).setBrand(brandchange);
                        break;
                    default:
                        System.out.println("商品编号输入错误");
                        break;
                }
            }

        }
    }
}

