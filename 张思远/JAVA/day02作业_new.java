1.什么是变量?变量的定义格式?要使用变量需要注意什么?
不确定的量； 变量类型 变量名 = 赋值；/ 变量类型 变量名； 变量名= 赋值；     注意使用前要进行初始化，注意不能超过变量类型的范围，注意赋值的值要对应变量类型。 变量命名不能使用关键字。


2.Java中的数据类型分几类?基本数据类型有哪些?
两类，基本数据类型和引用数据类型。 基本数据类型有：整型 byte short int long 浮点型 float double 布尔类型 boolean 字符类型 char



3.数据类型转换：
	隐式转换：由低级专向高级
	short s = 1;
	int i = s;
	强制转换：由高级专向低级
	int i = 1;
	short s = (short)i;



	面试题：
		第一题：
		byte b1=3,b2=4,b;
		b=b1+b2; // 应改为b= (byte) (b1+b2);
		b=3+4;
		哪句是编译失败的呢？为什么呢？
		第二句编译失败，因为b1 b2 默认为int类型，需要强制转换


		第二题：
		byte  by = 130;有没有问题?有问题如何解决?结果是多少呢?
		有问题，超过byte的范围，可以强制转换成byte 但是会损失精度， 结果为 127   byte  by = （byte）130;
		

		第三题:
		byte b = 10;
		b++;
		b = b + 1;
		哪句是编译失败的呢？为什么呢？
		第三句，因为1默认为int类型，和byte类型的数据进行运算时需要进行强制转换  b  = (byte)( b + 1)




4.常见的算术运算符有哪些?
	答： + - * / % > < != = 
	

5.常见的赋值运算符有哪些?
	答：
	(1)+=运算的作用是什么?
          b+=1;   相当于 b = b + 1;
		
	(2)扩展的赋值运算符有什么特点?
           默认强制转换，可能会存在精度损失
		
6. short s = 1; s = s + 1;有没有问题?如果有怎么解决?
                需要强制转换 s = (short)(s+1);
	
   short s = 1; s += 1;有没有问题?如果有怎么解决?
		没有问题
	
7.分析以下需求，并用代码实现：
	(1)已知一个三位数，请分别获取该三位数上每一位的数值
	(2)例如：123的个位、十位、百位，分别是3、2、1
	(3)打印格式"数字123的个位是 3, 十位是 2, 百位是 1"
import java.util.Scanner;

/**
 * (1)已知一个三位数，请分别获取该三位数上每一位的数值
 * (2)例如：123的个位、十位、百位，分别是3、2、1
 * (3)打印格式"数字123的个位是 3, 十位是 2, 百位是 1"
 * @author siyuan
 * @date 2020/12/14 14:43
 */
public class Digits {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入三位数");
        int input = sc.nextInt();
        int ones;//个位
        int twos;//十位
        int threes;//百位

        if (input >= 100 && input <=999){
            ones = input % 10;
            twos = input / 10 % 10;
            threes = input / 100;
            System.out.println("数字" + input + "的个位是:" + ones + ",十位是" + twos + ",百位是" + threes);
        }else {
            System.out.println("您输入的数不是三位数");
        }

    }
}


8.看程序说结果，请不要提前运行？
	public class Test1 {
		public static void main(String[] args) {
			 int x = 4;
  				
			 int y = (--x)+(x--)+(x*10);
  			
			 System.out.println("x = " + x + ",y = " + y);//2   26 

		}
	}


