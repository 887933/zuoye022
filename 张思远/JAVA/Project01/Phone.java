package Project;

/**
 * @author siyuan
 * @date 2020/12/26 21:40
 */
public class Phone {
    private String brand;
    private double size;
    private double price;
    private int number;

    public Phone(String brand, double size, double price, int number) {
        this.brand = brand;
        this.size = size;
        this.price = price;
        this.number = number;
    }

    public Phone() {
    }

    public String getBrand() {
        return brand;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
