package Project;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author siyuan
 * @date 2020/12/26 21:43
 */
public class View {
    public static void main(String[] args) {
        ArrayList<Phone> phones = new ArrayList();
        Phone phone1 = new Phone("小米", 5.5, 2500, 20);
        Phone phone2 = new Phone("华为", 5.8, 2700, 40);
        Phone phone3 = new Phone("oppo", 6.2, 3000, 40);
        phones.add(phone1);
        phones.add(phone2);
        phones.add(phone3);
        Scanner in = new Scanner(System.in);

        while (true) {
            System.out.println("********库存管理系统**************");
            System.out.println("1.查看库存清单");
            System.out.println("2.修改商品库存数量");
            System.out.println("3.清空库存");
            System.out.println("4.退出");

            System.out.println("请输入要执行的操作号");
            int enter = in.nextInt();
            if (enter == 1) {
                checkList(phones);
            } else if (enter == 2) {
                changeList(phones);

            } else if (enter == 3) {
                clearList(phones);
            } else if (enter == 4) {
                break;
            } else {
                System.out.println("请输入正确的数字！");
            }
        }
    }

    public static void checkList(ArrayList<Phone> phones) {
        if (phones.size() == 0) {
            System.out.println("暂无商品");
        }
        int sumNumber = 0;
        double sumPrice = 0;
        System.out.println("************查看库存清单**************");
        System.out.println("品牌型号       尺寸           价格    库存数");
        for (int i = 0; i < phones.size(); i++) {

            System.out.println(i + "品牌型号" + phones.get(i).getBrand() + ",尺寸是" + phones.get(i).getSize()
                    + "价格为" + phones.get(i).getPrice() + "库存数为" + phones.get(i).getNumber());

            for (int j = 0; j < phones.size(); j++) {

                sumNumber += phones.get(j).getNumber();
            }

            for (int j = 0; j < phones.size(); j++) {

                sumPrice += phones.get(j).getPrice();
            }
            sumPrice = sumPrice * sumNumber;


        }
        System.out.println("总库存数为" + sumNumber);
        System.out.println("总金额为" + sumPrice);

    }

    public static void changeList(ArrayList<Phone> phones) {
        Scanner in = new Scanner(System.in);
        System.out.println("修改商品的库存数量：");
        for (int i = 0; i < phones.size(); i++) {
            System.out.println("请输入" + phones.get(i).getBrand() + "商品库存数" + "///" + "当前库存数为" + phones.get(i).getNumber());
            int enterNumber = in.nextInt();
            phones.get(i).setNumber(enterNumber);

        }

    }
    public static void clearList(ArrayList<Phone> phones){
        Scanner in = new Scanner(System.in);
        checkList(phones);
        System.out.println("请选择清空的商品编号");
        int enterNumber = in.nextInt();

        System.out.println("确认清空吗？ y/n");
        String whether = in.next();

        if (whether.equals("y")){
            phones.remove(enterNumber);
        }else
        {
            checkList(phones);
        }

    }
}

