1.练习JDK的下载与安装
	(1)配置环境变量
	(2)了解JAVA语言跨平台的原理
	java语言本质上不能跨平台的，真正实现跨平台的是JVM，也就是Java虚拟机。写好的Java源文件通过javac命令编译生成class文件
	，然后JVM对class文件进行执行生成机器语言在平台上操作，java在不同的平台下都有对应的不同版本的JVM，JVM可以识别字节码文件，从而运行。
	(3)了解jdk和jre的区别和联系
	JDK是面向开发人员使用的SDK，它包括java的开发环境和运行环境。SDK是软件开发包，包括函数库，编译程序等。JRE是Java Runtime Enviroment
	是指java的运行环境，是面向java程序的使用者，，而不是开发者。如果安装了JDK，会发送你的电脑有两套JRE，一套位于jre，另外一套位于c盘，
	后面这套比前面的少了Server端的java虚拟机。
2.第一个HelloWorld敲10遍(10遍是个概数背下来为止)
	(1)练习编译和运行的过程
	(2)给HelloWorld添加注释(练习单行和多行注释)
3.练习常用DOS命令
4.下列哪种标识符,在java中是错误的？ D.E
	A:Class_Base12
	B:_Class$Base12
	C:$Class_12
	D:76Class$12
	E:class
	F:_Class $Base12
5.看程序说结果,不许直接运行吆？
	public class Demo
		{
			public static void main(String[] args) 
			{
				System.out.println(15);
				System.out.println(0B1111);
				System.out.println(0xF);
				System.out.println(017);
				
				System.out.println(true);
				System.out.println(false);
	
			}
		}

