package demo;

import java.util.ArrayList;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {

        ArrayList<XiaoMi> list = new ArrayList();
        XiaoMi x1 = new XiaoMi("小米1",5.51,1111,10);
        XiaoMi x2 = new XiaoMi("小米2",5.5,2222,20);
        XiaoMi x3 = new XiaoMi("小米3",5.52,3333,30);
        list.add(x1);
        list.add(x2);
        list.add(x3);
        tips(list);
    }
    public static void tips(ArrayList<XiaoMi> list){
        Scanner sc = new Scanner(System.in);
        System.out.println("--------库存管理系统-----------\n1.查看库存清单\n2.修改商品库存数量\n3.退出\n请输入要执行的操作编号：");
        int num = sc.nextInt();
        if (num == 1) {
            viewSystem(list);
            tips(list);
        }
        else if (num == 2){
            changeSystem(list);
            tips(list);
        }
        else if (num == 3){

        }
        else{
            System.out.println("请输入正确的操作代码!");
            tips(list);
        }

    }
    public static int totalMoney(ArrayList<XiaoMi> list){
        int sum = 0;
        for (int i = 0; i < list.size(); i++) {
            sum += list.get(i).numebr*list.get(i).price;
        }
        return sum;
    }
    public static int totalNumber(ArrayList<XiaoMi> list){
        int total = 0;
        for (int i = 0; i < list.size(); i++) {
            total += list.get(i).numebr;
        }
        return total;
    }
    public static void viewSystem(ArrayList<XiaoMi> list){
        String s="";
        for (int i = 0; i < list.size(); i++) {
            XiaoMi x = list.get(i);
            s+=x.name+"    "+x.size+"    "+x.price+"    "+x.numebr;
            s+="\n";
        }
        System.out.println("--------库存管理系统-----------\n品牌型号   尺寸     价格      库存\n"+s+"总库存数："
                +totalNumber(list)+"\n总金额："+totalMoney(list));
    }
    public static void changeSystem(ArrayList<XiaoMi> list){
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < list.size(); i++) {
            XiaoMi x = list.get(i);
            System.out.println("请输入"+x.name+"库存数");
            x.numebr = sc.nextInt();
        }
    }
}
