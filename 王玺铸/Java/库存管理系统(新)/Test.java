package demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Test {

    //每个用户的id属性
    static int id = 0;

    public static void main(String[] args) {

        //用户集合userList
        ArrayList<User> userList = new ArrayList();
        User u1 = new User("user01","123456");
        u1.setId(++id);
        User u2 = new User("user02","123456");
        u2.setId(++id);
        User u3 = new User("user03","123456");
        u3.setId(++id);

        userList.add(u1);

        userList.add(u2);
        userList.add(u3);

        //商品集合goodsList
        ArrayList<Goods> goodsList = new ArrayList();
        Goods x1 = new Goods("小米1",5.51,1111,10);
        Goods x2 = new Goods("小米2",5.5,2222,20);
        Goods x3 = new Goods("小米3",5.52,3333,30);

        goodsList.add(x1);
        goodsList.add(x2);
        goodsList.add(x3);

        //System.out.println(id);
        login(userList,goodsList);
    }

    //判断是否登录属性-log_in
    static boolean log_in = false;

    //调用login进入注册(登录)界面
    public static void login(ArrayList<User> userList,ArrayList<Goods> goodsList){
        while(!log_in){
            System.out.println("请输入用户账号密码进行登录，若无账号请先申请注册(输入1 登录； 输入2 注册； 输入3 退出)");
            Scanner sc = new Scanner(System.in);
            int num1 = sc.nextInt();

            if (num1 == 1){
                //登录
                System.out.println("请输入账号名：");
                String user_name = sc.next();
                System.out.println("请输入密码：");
                String user_password = sc.next();

                User u = new User(user_name,user_password);
                //判断账号密码是否匹配
                if(matching(u,userList) == true){

                    System.out.println("登录成功,进入商品操作界面");
                    log_in = true;
                    tips(goodsList,u);
                    //login(userList,goodsList);

                }
            }else if(num1 == 2){
                //注册
                System.out.println("请输入账号名：");
                String user_name = sc.next();
                System.out.println("请输入密码：");
                String user_password = sc.next();

                User u = new User(user_name,user_password);
                //判断是否存在该账号
                if(existOrNot(u,userList) == false){

                    u.setId(++id);
                    userList.add(u);

                    System.out.println("注册成功,返回登录界面");
                    login(userList,goodsList);
                }
            }else if(num1 == 3){
                break;
            }else{
                System.out.println("请输入正确的操作代码!");
            }
        }
    }

    //调用tips进入商品操作界面
    public static void tips(ArrayList<Goods> list,User user){

        Scanner sc = new Scanner(System.in);
        while(log_in){
            System.out.println("--------库存管理系统-----------\n1.查看库存清单\n2.修改商品信息\n3.删除货物\n4.添加货物\n5退出库存管理系统\n请输入要执行的操作编号：");
            int num2 = sc.nextInt();
            switch (num2){
                case 1:
                    //查看货物
                    viewGoods(list);
                    tips(list,user);
                    break;
                case 2:
                    //修改货物
                    changeGoods(user,list);
                    tips(list,user);
                    break;
                case 3:
                    //删除货物
                    deleteGoods(user,list);
                    break;
                case 4:
                    //添加货物
                    addGoods(user,list);
                    break;
                case 5:
                    //退出系统
                    log_in = false;
                    break;
                default:
                    System.out.println("请输入正确的操作代码!");
                    tips(list,user);
            }
        }
    }

    //计算货物总价值
    public static double totalMoney(ArrayList<Goods> list){
        double sum = 0;
        for (int i = 0; i < list.size(); i++) {
            sum += list.get(i).getNumber()*list.get(i).getPrice();
        }
        return sum;
    }

    //计算货物总数
    public static int totalNumber(ArrayList<Goods> list){
        int total = 0;
        for (int i = 0; i < list.size(); i++) {
            total += list.get(i).getNumber();
        }
        return total;
    }

    //查看货物信息
    public static void viewGoods(ArrayList<Goods> list){
        String s="";
        for (int i = 0; i < list.size(); i++) {
            Goods x = list.get(i);
            s+=x.getName()+"    "+x.getSize()+"    "+x.getPrice()+"    "+x.getNumber();
            s+="\n";
        }
        System.out.println("--------库存管理系统-----------\n品牌型号   尺寸     价格      库存\n"+s+"总库存数："
                +totalNumber(list)+"\n总金额："+totalMoney(list));
    }

    //修改货物信息
    public static void changeGoods(User user,ArrayList<Goods> list){

        Scanner sc = new Scanner(System.in);

        System.out.println("请输入需要修改的属性");
        Object obj1 = sc.next();

        System.out.println("请输入修改后的属性值");
        Object obj2 = sc.next();

        if (obj1.equals("name")){
            //修改name属性

            System.out.println("请根据提示输入操作指令 （输入 1：修改全部商品的对应信息； 输入2：修改个别商品对应信息）");
            int num4 = sc.nextInt();
            if (num4 == 1){
                for (int i=0;i<list.size();i++){
                    list.get(i).setName(obj2.toString());
                }
                System.out.println("用户编号"+user.getId()+"修改所有商品的name属性为"+obj2.toString());
            }else if(num4 == 2){

                System.out.println("请输入要修改的商品名：");
                String name = sc.next();
                for (int i=0;i<list.size();i++){
                    if (list.get(i).getName().equals(name)){
                        list.get(i).setName(obj2.toString());
                    }
                }
                System.out.println("用户编号"+user.getId()+"修改"+name+"商品的name属性为"+obj2.toString());
            }else{
                System.out.println("请输入正确的操作代码！");
            }
        }else if(obj1.equals("size")){
            //修改size属性

            System.out.println("请根据提示输入操作指令 （输入 1：修改全部商品的对应信息； 输入2：修改个别商品对应信息）");
            int num4 = sc.nextInt();
            if (num4 == 1){
                for (int i=0;i<list.size();i++){
                    list.get(i).setSize(Double.parseDouble(obj2.toString()));
                }
                System.out.println("用户编号"+user.getId()+"修改所有商品的size属性为"+obj2.toString());
            }else if(num4 == 2){

                System.out.println("请输入要修改的商品名：");
                String name = sc.next();
                for (int i=0;i<list.size();i++){
                    if (list.get(i).getName().equals(name)){
                        list.get(i).setSize(Double.parseDouble(obj2.toString()));
                    }
                }
                System.out.println("用户编号"+user.getId()+"修改"+name+"商品的size属性为"+obj2.toString());
            }else{
                System.out.println("请输入正确的操作代码！");
            }
        }else if(obj1.equals("price")){
            //修改price属性

            System.out.println("请根据提示输入操作指令 （输入 1：修改全部商品的对应信息； 输入2：修改个别商品对应信息）");
            int num4 = sc.nextInt();
            if (num4 == 1){
                for (int i=0;i<list.size();i++){
                    list.get(i).setPrice(Double.parseDouble(obj2.toString()));
                }
                System.out.println("用户编号"+user.getId()+"修改所有商品的price属性为"+obj2.toString());
            }else if(num4 == 2){

                System.out.println("请输入要修改的商品名：");
                String name = sc.next();
                for (int i=0;i<list.size();i++){
                    if (list.get(i).getName().equals(name)){
                        list.get(i).setPrice(Double.parseDouble(obj2.toString()));
                    }
                }
                System.out.println("用户编号"+user.getId()+"修改"+name+"商品的price属性为"+obj2.toString());
            }else{
                System.out.println("请输入正确的操作代码！");
            }
        }else if(obj1.equals("number")){
            //修改number属性

            System.out.println("请根据提示输入操作指令 （输入 1：修改全部商品的对应信息； 输入2：修改个别商品对应信息）");
            int num4 = sc.nextInt();
            if (num4 == 1){
                for (int i=0;i<list.size();i++){
                    list.get(i).setNumber(Integer.parseInt(obj2.toString()));
                }
                System.out.println("用户编号"+user.getId()+"修改所有商品的number属性为"+obj2.toString());
            }else if(num4 == 2){

                System.out.println("请输入要修改的商品名：");
                String name = sc.next();
                for (int i=0;i<list.size();i++){
                    if (list.get(i).getName().equals(name)){
                        list.get(i).setNumber(Integer.parseInt(obj2.toString()));
                    }
                }
                System.out.println("用户编号"+user.getId()+"修改"+name+"商品的number属性为"+obj2.toString());
            }else{
                System.out.println("请输入正确的操作代码！");
            }
        }else{
            System.out.println("请输入正确的属性");
        }
    }

    //删除货物
    public static void deleteGoods(User user,ArrayList<Goods> list){
        Scanner sc = new Scanner(System.in);
        System.out.println("输入1：删除全部货物信息； 输入2：删除指定货物信息");
        int num3 = sc.nextInt();
        if (num3 == 1){
            list.clear();
            System.out.println("用户编号"+user.getId()+"删除了全部货物信息");
        }else if (num3 == 2){
            System.out.println("请输入要删除货物的名字：");
            String name = sc.next();
            for (int i=0;i<list.size();i++){
                if (list.get(i).getName().equals(name)){
                    System.out.println("用户编号"+user.getId()+"指定删除了货物"+list.get(i).getName());
                    list.remove(i);
                    break;
                }
            }
        }else{
            System.out.println("请输入正确操作代码!");
        }
    }

    //添加货物
    public static void addGoods(User user,ArrayList<Goods> list){
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入添加货物的信息");

        System.out.println("请输入添加货物的名字");
        String name = sc.next();

        System.out.println("请输入添加货物的尺寸");
        double size = sc.nextDouble();

        System.out.println("请输入添加货物的单价");
        double price = sc.nextDouble();

        System.out.println("请输入添加货物的数量");
        int number = sc.nextInt();

        Goods goods = new Goods(name,size,price,number);
        list.add(goods);
        System.out.println("用户编号"+user.getId()+"成功添加新货物"+goods.getName());

    }

    //判断账号名是否重复（注册）
    public static boolean existOrNot(User user,ArrayList<User> userList){
        for(int i=0;i<userList.size();i++){
            if (user.getName().equals(userList.get(i).getName())){
                System.out.println("账号重复，请重新输入账号名");
                return true;
            }
        }
        return false;
    }

    //判断账号密码是否匹配（登录）
    public static boolean matching(User user,ArrayList<User> userList) {
        for (int i = 0; i < userList.size(); i++) {
            if (user.getName().equals(userList.get(i).getName())) {
                if (user.getPassword().equals(userList.get(i).getPassword())) {
                    //让登录对象的id与真实对象一致
                    user.setId(userList.get(i).getId());
                    return true;
                } else {
                    System.out.println("密码错误！返回主界面");
                    return false;
                }
            }
        }
        System.out.println("账号错误！返回主界面");
        return false;
    }
}
