package demo;

public class Goods {
    private String name;
    private double size;
    private double price;
    private int number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Goods(){

    }

    public Goods(String name, double size, double price, int number){
        this.name = name;
        this.size = size;
        this.price = price;
        this.number = number;
    }
}
