package zuoyeDemo02;

/**
 * @author mengmeng
 * @date 2021/3/14 22:38
 */
public class EmployeeTest {
    public static void main(String[] args) {
        Manager manager = new Manager("张三", "123456", 10000.0, 2000.0);
        Programmer programmer = new Programmer("码农", "000001", 6000.0);
        manager.work();
        programmer.work();
    }
}
