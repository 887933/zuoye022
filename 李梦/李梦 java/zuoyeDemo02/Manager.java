package zuoyeDemo02;


/**
 * @author mengmeng
 * @date 2021/3/14 22:30
 */
public class Manager  extends Employee {
    //奖金
    private double bonus;

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public Manager(String name, String num, double salary, double bonus) {
        super(name, num, salary);
        this.bonus = bonus;
    }
    public Manager(){
        super();
    }

    public void  work(){

        System.out.println(super.getName() + "的工号" + super.getNum() + ",工资是" + super.getSalary() + ",奖金是" + bonus);
    }
}
