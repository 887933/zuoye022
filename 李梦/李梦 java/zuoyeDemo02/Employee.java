package zuoyeDemo02;

/**
 * @author mengmeng
 * @date 2021/3/14 22:30
 */
public class Employee {
    private String name;
    private String num;
    private double salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public double getSalary() {
        return salary;
    }
    public void work(){
        System.out.print(name + "的工号" + num + ",工资是" + salary);
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Employee(String name, String num, double salary) {
        this.name = name;
        this.num = num;
        this.salary = salary;
    }

    public Employee() {
    }
}
