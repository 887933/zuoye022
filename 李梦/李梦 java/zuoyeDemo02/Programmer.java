package zuoyeDemo02;

/**
 * @author mengmeng
 * @date 2021/3/14 22:30
 */
public class Programmer  extends Employee{
    public Programmer(String name, String num, double salary) {
        super(name, num, salary);
    }

    public Programmer() {
    }
    public void work(){
        System.out.println(super.getName() + "的工号" + super.getNum() + ",工资是" + super.getSalary());
    }
}
