package PersonDemo;

/**
 * @author mengmeng
 * @date 2021/3/14 22:48
 */
public class Student extends Person{
    public Student() {
    }

    public Student(String name, int age) {
        super(name, age);
    }
    public void study(){
        System.out.println("年龄是：" + super.getAge() + "," + super.getName() + "同学在上课");
    }

    @Override
    public void eat() {
        System.out.println(super.getName() + "在吃饭");
    }
}
