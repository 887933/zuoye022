package PersonDemo;

/**
 * @author mengmeng
 * @date 2021/3/14 22:48
 */
public class Teacher extends Person{
    public Teacher() {
    }

    public Teacher(String name, int age) {
        super(name, age);
    }
    public void teach(){
        System.out.println("年龄是：" + super.getAge() + "," + super.getName() + "老师在讲课");
    }
    @Override
    public void eat() {
        System.out.println(super.getName() + "在吃饭");
    }
}
