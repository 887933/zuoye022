package PersonDemo;

/**
 * @author mengmeng
 * @date 2021/3/14 22:57
 */
public class Test {
    public static void main(String[] args) {
        Student student = new Student("小明", 20);
        Teacher teacher = new Teacher( "老王", 50);
        student.study();
        teacher.teach();
        student.eat();
        teacher.eat();
    }
}
