package zuoye01Demo;

/**
 * @author mengmeng
 * @date 2021/3/14 22:24
 */
public class Person {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public Person( ) {
    }
    public void open(){
        System.out.println("打开冰箱");
    }
    public void close(){
        System.out.println("关上冰箱");
    }
}
