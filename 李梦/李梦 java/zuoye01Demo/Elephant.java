package zuoye01Demo;

/**
 * @author mengmeng
 * @date 2021/3/14 22:15
 *
 * 把大象装进冰箱
 */
public class Elephant {

    private String name;

    public void enterIce(){
        System.out.println("大象被关进冰箱");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Elephant(String name) {
        this.name = name;

    }
    public Elephant() {

    }
}
