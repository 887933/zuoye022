package zuoye01Demo;

/**
 * @author mengmeng
 * @date 2021/3/14 22:18
 * 冰箱
 */
public class Refrigerator {
    private String brand;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Refrigerator(String brand) {
        this.brand = brand;
    }
    public Refrigerator( ) {
    }
    public void open(){
        System.out.println("冰箱门被打开");
    }
    public void close(){
        System.out.println("冰箱门被关上");
    }
}
