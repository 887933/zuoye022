package zuoye01Demo;

/**
 * @author mengmeng
 * @date 2021/3/14 22:25
 */
public class Test {
    public static void main(String[] args) {
        Elephant elephant = new Elephant("大大象");
        Refrigerator refrigerator = new Refrigerator("海尔");
        Person person = new Person("路人甲", 15);
        person.open();
        refrigerator.open();
        elephant.enterIce();
        person.close();
        refrigerator.close();
    }
}
