package AbstractDemo02;

/**
 * @author mengmeng
 * @date 2021/3/14 23:10
 */
public abstract class AbstractAnimal {
    private String color;
    private int legCount;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLegCount() {
        return legCount;
    }

    public void setLegCount(int legCount) {
        this.legCount = legCount;
    }

    public AbstractAnimal(String color, int legCount) {
        this.color = color;
        this.legCount = legCount;
    }

    public AbstractAnimal() {
    }
    public abstract void eat();
    //特殊行为
     public abstract void specialBehavior();

}
