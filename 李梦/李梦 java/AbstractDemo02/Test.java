package AbstractDemo02;

/**
 * @author mengmeng
 * @date 2021/3/14 23:12
 */
public class Test {
    public static void main(String[] args) {
        Dog dog = new Dog("白色", 4);
        Cat cat = new Cat("黄色",4);
        dog.eat();
        dog.specialBehavior();
        cat.eat();
        cat.specialBehavior();
    }
}
