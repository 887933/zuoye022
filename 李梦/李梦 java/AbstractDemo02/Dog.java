package AbstractDemo02;

/**
 * @author mengmeng
 * @date 2021/3/14 23:10
 */
public class Dog extends AbstractAnimal{
    public Dog(String color, int legCount) {
        super(color, legCount);
    }

    public Dog() {
    }

    @Override
    public void eat() {
        System.out.println(super.getColor() + "的狗在吃饭");
    }

    @Override
    public void specialBehavior() {
        System.out.println("狗的特殊行为：看家");
    }
}
