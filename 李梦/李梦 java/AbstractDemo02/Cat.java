package AbstractDemo02;



/**
 * @author mengmeng
 * @date 2021/3/14 23:10
 */
public class Cat extends AbstractAnimal {
    public Cat(String color, int legCount) {
        super(color, legCount);
    }

    public Cat() {
    }

    @Override
    public void eat() {
        System.out.println(super.getColor() + "的猫在吃饭");
    }

    @Override
    public void specialBehavior() {
        System.out.println("猫的特殊行为：抓老鼠");
    }
}
