package AbstractDemo;

/**
 * @author mengmeng
 * @date 2021/3/14 23:01
 */
public class JcbTeacher extends AbstractTeacher{
    public JcbTeacher(String name, int age) {
        super(name, age);
    }

    public JcbTeacher() {
    }

    @Override
    public void Teach() {
        System.out.println(super.getName() + "讲述JavaEE");
    }
}
