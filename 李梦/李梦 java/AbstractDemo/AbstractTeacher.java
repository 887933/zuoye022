package AbstractDemo;

/**
 * @author mengmeng
 * @date 2021/3/14 23:00
 */
public abstract  class AbstractTeacher {
    private String name;
    private int age;

    public AbstractTeacher(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public AbstractTeacher() {

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public abstract void Teach();

}
