package AbstractDemo;

/**
 * @author mengmeng
 * @date 2021/3/14 23:01
 */
public class JybTeacher extends AbstractTeacher{
    public JybTeacher(String name, int age) {
        super(name, age);
    }

    public JybTeacher() {
    }

    @Override
    public void Teach() {
        System.out.println(super.getName() + "讲述Android");
    }
}
