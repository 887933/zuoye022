package AbstractDemo;

/**
 * @author mengmeng
 * @date 2021/3/14 23:03
 */
public class Test {
    public static void main(String[] args) {
        JcbTeacher jcbTeacher = new JcbTeacher("基础班老师", 25);
        JybTeacher jybTeacher = new JybTeacher("就业班老师", 35);
        jcbTeacher.Teach();
        jybTeacher.Teach();
    }
}
