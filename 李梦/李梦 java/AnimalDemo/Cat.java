package AnimalDemo;

/**
 * @author mengmeng
 * @date 2021/3/14 22:42
 */
public class Cat extends Animal{
    public Cat(String color, int legCount) {
        super(color, legCount);
    }

    public Cat() {
    }
    public void catchMouse(){
        System.out.println("猫的特殊行为：抓老鼠");
    }

    @Override
    public void eat() {
        System.out.println("猫吃饭");
    }
    public void show(){
        System.out.println("猫颜色为" + super.getColor() + "，腿数为" + super.getLegCount());
    }
}
