package AnimalDemo;

/**
 * @author mengmeng
 * @date 2021/3/14 22:42
 */
public class Dog extends Animal{
    public Dog(String color, int legCount) {
        super(color, legCount);
    }

    public Dog() {
    }
    public void lookHome(){
        System.out.println("狗的特殊行为：看家");
    }

    @Override
    public void eat() {
        System.out.println("狗吃饭");
    }
    public void show(){
        System.out.println("狗颜色为" + super.getColor() + "，腿数为" + super.getLegCount());
    }
}
