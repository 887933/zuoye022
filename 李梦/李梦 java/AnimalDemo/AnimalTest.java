package AnimalDemo;

/**
 * @author mengmeng
 * @date 2021/3/14 22:44
 */
public class AnimalTest {
    public static void main(String[] args) {
        Dog dog = new Dog("白色", 4);
        Cat cat = new Cat("黄色",4);
        cat.catchMouse();
        dog.lookHome();
        dog.eat();
        dog.show();
        cat.eat();
        cat.show();
    }
}
