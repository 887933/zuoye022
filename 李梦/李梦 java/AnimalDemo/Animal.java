package AnimalDemo;

/**
 * @author mengmeng
 * @date 2021/3/14 22:42
 */
public class Animal {
    private String color;
    private int legCount;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLegCount() {
        return legCount;
    }

    public void setLegCount(int legCount) {
        this.legCount = legCount;
    }

    public Animal(String color, int legCount) {
        this.color = color;
        this.legCount = legCount;
    }

    public Animal() {
    }
    public void eat(){
        System.out.println("动物吃饭");
    }
}
