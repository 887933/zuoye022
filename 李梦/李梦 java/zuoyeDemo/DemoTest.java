package zuoyeDemo;

import java.util.Scanner;

/**
 * @author mengmeng
 * @date 2021/3/14 22:02
 */
public class DemoTest {
    public static void main(String[] args) {
        Demo demo = new Demo();
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入两个数");
        int num1 = scanner.nextInt();
        int num2 = scanner.nextInt();
        int sum = demo.getSum(num1, num2);
        System.out.println("两数和" + sum);
        boolean equals = demo.isEquals(num1, num2);
        if (equals){
            System.out.println("两数相等");
        }else {
            System.out.println("两数不相等");
        }
        demo.getCfb();
    }


}
