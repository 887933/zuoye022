package zuoyeDemo;

/**
 * @author mengmeng
 * @date 2021/3/14 22:02
 */
public class Demo {


    public int getSum(int num1, int num2){
        int sum = num1 + num2;
        return sum;
    }
    public boolean isEquals(int num1, int num2){
        return num1 == num2;
    }

    public void getCfb(){
        for (int i = 1; i <= 9; i++){

            for (int j = 1; j <= i ; j++) {
                int result =   j * i;
                System.out.print(j + "*" + i +"=" + result + " ");
            }
            System.out.println();
        }
    }
}
