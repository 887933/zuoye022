package demo01;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author mengmeng
 * @date 2020/12/30 20:28
 */
public class PhoneTest {
    public static void main(String[] args) {
        ArrayList<Phone> phone = new ArrayList();
        addPhone(phone);
        boolean isExit = true;
        while(isExit)
        {
            System.out.println("\n-------------库存管理系统--------------------");
            System.out.println("1 查看库存清单");
            System.out.println("2 修改商品库存数量");
            System.out.println("3 删除某个商品");
            System.out.println("4 清空");
            System.out.println("5 退出");
            System.out.print("请选择（1-3）执行操作：");
            Scanner sc = new Scanner(System.in);
            int menu = sc.nextInt();
            switch (menu){
                case 1:
                    listALLPhone(phone);
                    break;
                case 2:
                    modifyCount(phone);
                    break;
                case 3:
                    deletePhone(phone);
                    System.out.println("删除成功");
                    break;
                case 4:
                    clearAll(phone);
                    System.out.println("清除成功");
                    break;
                case 5:
                    isExit=false;
                    System.out.println("退出成功");
                    break;
                default:
                    System.out.println("找不到该指令");
                    break;
            }
        }
    }
    public static  void addPhone(ArrayList<Phone> phone){
        System.out.println("-------------添加商品--------------------");
        Scanner sc= new Scanner(System.in);
        for(int i=0;i<3;i++){
            Phone p = new Phone();
            System.out.println("请输入第"+(i+1)+"个手机品牌型号");
            p.name=sc.next();
            System.out.println("请输入第"+(i+1)+"个手机尺寸");
            p.size=sc.nextFloat();
            System.out.println("请输入第"+(i+1)+"个手机价格");
            p.price=sc.nextDouble();
            System.out.println("请输入第"+(i+1)+"个手机库存数");
            p.count=sc.nextInt();
            phone.add(p);
        }
    }
    public static  void listALLPhone(ArrayList<Phone> phone){
        System.out.println("-------------查看库存清单--------------------");
        System.out.println("品牌型号\t\t尺寸\t\t价格\t\t库存数");
        int count = 0;
        double price = 0;
        for(int i=0;i<phone.size();i++){
            Phone p = phone.get(i);
            System.out.println(p.name+'\t'+'\t'+p.size+'\t'+'\t'+p.price+'\t'+'\t'+p.count);
            count+=p.count;
            price+=p.price*p.count;
        }
        System.out.println("总库存数："+count);
        System.out.println("总金额："+price);
    }
    public static  void modifyCount(ArrayList<Phone> phone){
        System.out.println("-------------修改商品库存数量--------------------");
        Scanner sc= new Scanner(System.in);
        int count=0;
        for(int i=0;i<phone.size();i++){
            Phone p = phone.get(i);
            System.out.println("请输入"+p.name+"的新库存数");
            count = sc.nextInt();
            p.count=count;
        }

    }
    public static void deletePhone(ArrayList<Phone> phone){
        System.out.println("-------------删除商品--------------------");
        Scanner sc= new Scanner(System.in);
        int num=0;
        for(int i=0;i<phone.size();i++){
            System.out.println("请输入商品编号删除");
            num= sc.nextInt();
            if (num<phone.size()) {
                phone.remove(num);
                break;
            }
        }
        for(int i=0;i<phone.size();i++){
            Phone p = phone.get(i);
        }
    }
    public static void clearAll(ArrayList<Phone> phone){
        System.out.println("-------------清空商品--------------------");
        phone.clear();
    }
}
