package AbstractDemo01;

/**
 * @author mengmeng
 * @date 2021/3/14 23:05
 */
public class JcbStudent extends AbstractStudent{
    public JcbStudent(String name, int age) {
        super(name, age);
    }

    public JcbStudent() {
    }

    @Override
    public void Study() {
        System.out.println(super.getName() + "学习JavaEE");
    }
}
