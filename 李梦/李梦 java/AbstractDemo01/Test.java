package AbstractDemo01;


/**
 * @author mengmeng
 * @date 2021/3/14 23:08
 */
public class Test {
    public static void main(String[] args) {
        JcbStudent jcbStudent = new JcbStudent("基础班学生", 20);
        JybStudent jybStudent = new JybStudent("就业办学生", 22);
        jybStudent.Study();
        jcbStudent.Study();
    }


}
