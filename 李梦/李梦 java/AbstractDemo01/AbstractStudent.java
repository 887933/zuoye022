package AbstractDemo01;

/**
 * @author mengmeng
 * @date 2021/3/14 23:05
 */
public abstract  class AbstractStudent {
    private String name;
    private int age;

    public AbstractStudent(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public AbstractStudent() {

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public abstract void Study();
}
