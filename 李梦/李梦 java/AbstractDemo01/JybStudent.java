package AbstractDemo01;

/**
 * @author mengmeng
 * @date 2021/3/14 23:05
 */
public class JybStudent extends AbstractStudent{
    public JybStudent(String name, int age) {
        super(name, age);
    }

    public JybStudent() {
    }

    @Override
    public void Study() {
        System.out.println(super.getName() + "学习Android");
    }
}
