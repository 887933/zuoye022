1.什么是变量?变量的定义格式?要使用变量需要注意什么?
变量是程序运行的时候，内容可以发生变化的量
数据类型 变量名称

注意事项
1.变量之间名称不能重复
2.float与long的后缀要加上
3.没有进行赋值的变量不能直接使用，这里指局部变量
4.变量使用不能超过作用域的范围，即从定义变量的一行开始,一致到所属的大括号结束为止.
5.可以统一个语句来创建多个变量,但是一般情况下不要这样写
6.如果使用byte和short类型的变量,那么右侧的数据不要超过左侧类型的范围

2.Java中的数据类型分几类?基本数据类型有哪些?
基本数据类型和引用数据类型，基本数据类型：数值型、浮点型、布尔型、字符型

3.数据类型转换：
	隐式转换：由低级专向高级
	强制转换：由高级专向低级


	面试题：
		第一题：
		byte b1=3,b2=4,b;
		b=b1+b2;
		b=3+4;
		哪句是编译失败的呢？为什么呢？
		b=b1+b2;编译失败，因为b1与b2是变量，编译时不确定类型，会将结果视为int处理
		第二题：
		byte  by = 130;有没有问题?有问题如何解决?结果是多少呢?
		有问题，将byte改为int，如果byte  by = （byte）130，结果是负数-126

		第三题:
		byte b = 10;
		b++;
		b = b + 1;
		哪句是编译失败的呢？为什么呢？
		b = b + 1;，因为b是byte，1是int，运算结果为int，所以编译不通过,改为b =(byte ) b + 1;
4.常见的算术运算符有哪些?
	答：+，-，*，/，%，--，++
	

5.常见的赋值运算符有哪些?
	答：
	(1)+=运算的作用是什么?
		sum+=i 即 sum=sum+i;
	(2)扩展的赋值运算符有什么特点?
	用扩展运算符就不用考虑类型转化，不会改变原有的数据类型吧，在参与运算时不会提示可能损失精度错误

6. short s = 1; s = s + 1;有没有问题?如果有怎么解决?
	有问题，s+1运算后为int，要转换，即s=(short)(s+1)
   short s = 1; s += 1;有没有问题?如果有怎么解决?
	没有问题，
7.分析以下需求，并用代码实现：
	(1)已知一个三位数，请分别获取该三位数上每一位的数值
	
	
	(2)例如：123的个位、十位、百位，分别是3、2、1
	(3)打印格式"数字123的个位是 3, 十位是 2, 百位是 1"
 public static void main(String[] args){
        int onesPlace=0;
        int tenPlace=0;
        int hundredPlace=0;
        System.out.println("请输入一个三位数");
        Scanner sc = new Scanner(System.in);
        int enterNum = sc.nextInt();
        while (true) {
            if (enterNum >= 100 && enterNum <= 999) {
                onesPlace = enterNum % 10;
                tenPlace = enterNum / 10 % 10;
                hundredPlace = enterNum / 100;
                break;
            } else {
                System.out.println("请重新输入一个三位数");
                enterNum = sc.nextInt();
            }
        }
        System.out.print("个位是："+onesPlace+",");
        System.out.print("十位是："+tenPlace+",");
        System.out.print("百位是："+hundredPlace);
}
8.看程序说结果，请不要提前运行？
	public class Test1 {
		public static void main(String[] args) {
			 int x = 4;
  				
			 int y = (--x)+(x--)+(x*10);
  			
			 System.out.println("x = " + x + ",y = " + y);

		}
	}
y输出为26，x为2

