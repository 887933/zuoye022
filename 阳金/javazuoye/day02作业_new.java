1.什么是变量?变量的定义格式?要使用变量需要注意什么?
变量是一个不定的量，用来存数据和取数据。
变量定义： 变量类型 变量名 = 变量值  变量名字不能数字开头 且不能与关键词重名。
使用变量之前要进行初始化、赋值注意数据类型和范围是否匹配。

2.Java中的数据类型分几类?基本数据类型有哪些?
基本数据类型：整形、浮点型、字符型、布尔型
引用数据类型：数组、类、接口

3.数据类型转换：
	隐式转换：由低级专向高级
	强制转换：由高级专向低级


	面试题：
		第一题：
		byte b1=3,b2=4,b;
		b=b1+b2;
		b=3+4;
		哪句是编译失败的呢？为什么呢？
第二局编译失败、因为b1、b2的值为int型 b为byte型数据 相加之后数据类型不匹配（因为不知道相加之后是否超过byte类型的范围）
3+4为7没有超出byte类型的范围所以不会报错

		第二题：
		byte  by = 130;有没有问题?有问题如何解决?结果是多少呢?
有问题 byte类型数据范围-128~127 130超出了这个范围 可以将byte改为更大的数据类型 short或者int
结果都为130
byte  by = (byte)130;结果为-126

		第三题:
		byte b = 10;
		b++;
		b = b + 1;
		哪句是编译失败的呢？为什么呢？
第三句会编译失败 因为1默认为int类型数据、b=b+1 类型为int 无法确定结果是否超出byte类型范围
可扩大定义范围 使用更大的数据类型
强制转换b=(byte)(b+1);

4.常见的算术运算符有哪些?
	答：
	+ - * / % ++ -- == != > < >= <= && ||  ! ^ & |
	&& || 最左侧为false则不管后面如何  整个都为false

5.常见的赋值运算符有哪些?
	答：
	(1)+=运算的作用是什么?
	自身相加 a=a+1 等价于a+=1
		
	(2)扩展的赋值运算符有什么特点?
	一旦运算当中有不同类型的数据,那么结果将会是数据类型范围大的那种
		
6. short s = 1; s = s + 1;有没有问题?如果有怎么解决?
	 因为1默认为int类型数据、s=s+1 类型为int 无法确定结果是否超出short类型范围
	可扩大定义范围 使用更大的数据类型
	强制转换s=(short)(s+1);
	
   short s = 1; s += 1;有没有问题?如果有怎么解决?
	没有问题。
	
7.分析以下需求，并用代码实现：
	(1)已知一个三位数，请分别获取该三位数上每一位的数值
	(2)例如：123的个位、十位、百位，分别是3、2、1
	(3)打印格式"数字123的个位是 3, 十位是 2, 百位是 1"
	package demo;
public class HelloWorld {
	public static void main(String[] args){
		System.out.println("Hello World!");
		int num1=789;
		int baiwei=1;
		int shiwei=1;
		int gewei=1;
		int sum=1;
		baiwei=num1%10;//9
		sum=num1/10;//78
		shiwei=sum%10;//8
		gewei=sum/10;//7
		System.out.println("个位是"+gewei+"十位是"+shiwei+"百位是"+baiwei);
	}
}

8.看程序说结果，请不要提前运行？
	public class Test1 {
		public static void main(String[] args) {
			 int x = 4;
  				
			 int y = (--x)+(x--)+(x*10);
  			
			 System.out.println("x = " + x + ",y = " + y);
		}
	}
		结果为：x=2 y=26

